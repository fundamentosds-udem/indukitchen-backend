package org.indukitchen.backend.controller;


import org.indukitchen.backend.model.Libro;
import org.indukitchen.backend.service.LibroService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/libros")
public class LibrosController {

    private final LibroService libroService;

    public LibrosController(LibroService libroService) {
        this.libroService = libroService;
    }

    @GetMapping("/ping")
    public String ping(){
        return "pong";
    }

    @GetMapping
    public List<Libro> consultarLibros(){
        return libroService.consultarLibros();
    }

    @PostMapping
    public Libro guardarLibro(@RequestBody Libro libro){
        return libroService.guardarLibro(libro);
    }
}
