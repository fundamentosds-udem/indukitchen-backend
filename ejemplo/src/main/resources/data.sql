INSERT INTO autores (id, nombres) VALUES ('1', 'Autor 1');
INSERT INTO autores (id, nombres) VALUES ('2', 'Autor 2');

INSERT INTO libros (isbn, nombre, autor_id) VALUES ('001', 'Libro 001', '1');
INSERT INTO libros (isbn, nombre, autor_id) VALUES ('002', 'Libro 002', '1');
INSERT INTO libros (isbn, nombre, autor_id) VALUES ('003', 'Libro 003', '1');
INSERT INTO libros (isbn, nombre, autor_id) VALUES ('004', 'Libro 004', '2');
INSERT INTO libros (isbn, nombre, autor_id) VALUES ('005', 'Libro 005', '2');
INSERT INTO libros (isbn, nombre, autor_id) VALUES ('006', 'Libro 006', '2');

